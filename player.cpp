#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
  // Will be set to true in test_minimax.cpp.
  testingMinimax = false;

  own = side;
  other = (side == BLACK) ? WHITE: BLACK;
  board = Board();
    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

  /*
  if (msLeft <= 0) {
    return NULL;
  }
  */
  
  if (opponentsMove != NULL){
    board.doMove(opponentsMove, other);
  }

  Move *bestMove = minimax(&board, own, 2);   

  board.doMove(bestMove, own);
  return bestMove;
}


Move* Player::minimax(Board *board, Side side, int level){
  
  vector<Move*> moves = board->hasMoves(side);
  int num = moves.size();

  if (num == 0){
    return NULL;
  }
  else if (num == 1){
    return moves.at(0);
  }

  if (side == own){
    Move *maxMove = NULL;
    int maxScore = -64;
    Board *testBoard;
    
    for (int i = 0; i < num; i++){
      Move *move = moves.at(i);
      testBoard = board->copy();
      testBoard->doMove(move, side);

      int score;
      
      if(level > 1){
	Move *temp = minimax(testBoard, other, level - 1);

	if (temp == NULL){
	  return move;
	}
	
	testBoard->doMove(temp, other);
	score = moveScore(testBoard, temp, other);
      }
      
      else {
	score = moveScore(testBoard, move, side);
      }
      
      if (score > maxScore){
	if (maxMove != NULL){
	  delete maxMove;
	}
	maxScore = score;
	maxMove = move;
      }
      else {
	delete move;
      }

      delete testBoard;
    }

    return maxMove;
  }
  
  else {
    Move *minMove = NULL;
    int minScore = 64;
    Board *testBoard;
    
    for (int i = 0; i < num; i++){
      Move *move = moves.at(i);
      testBoard = board->copy();
      testBoard->doMove(move, side);

      int score;
      
      if(level > 1){
	Move *temp = minimax(testBoard, own, level - 1);

	if (temp == NULL){
	  return move;
	}
	
	testBoard->doMove(temp, own);
	score = moveScore(testBoard, temp, own);
      }
      
      else {
	score = moveScore(testBoard, move, side);
      }

      if (score < minScore){
	if (minMove != NULL){
	  delete minMove;
	}
	minScore = score;
	minMove = move;
      }
      else {
	delete move;
      }

      delete testBoard;
    }
    
    return minMove;
  }
}

int Player::moveScore(Board *testBoard, Move *move, Side side){
  int score = testBoard->count(own) - testBoard->count(other);

  if (testingMinimax == false){
    int X = move->getX();
    int Y = move->getY();

    if (side == own){
      if ((X == 0 || X == 7) && (Y == 0 || Y == 7)){
	score += 20;
      }
      else if (X == 0 || X == 7 || Y == 0 || Y == 7){
	if (((X == 0 || X == 7) && (Y == 1 || Y == 6)) ||
	    ((Y == 0 || Y == 7) && (X == 1 || X == 6))){
	  score -= 10;
	}
	else {
	  score += 10;
	}
      }
      else if ((X == 1 || X == 6) && (Y == 1 || Y == 6)){
	score -= 20;
      }
    }
    else {
      if ((X == 0 || X == 7) && (Y == 0 || Y == 7)){
	score -= 20;
      }
      else if (X == 0 || X == 7 || Y == 0 || Y == 7){
	if (((X == 0 || X == 7) && (Y == 1 || Y == 6)) ||
	    ((Y == 0 || Y == 7) && (X == 1 || X == 6))){
	  score += 10;
	}
	else {
	  score -= 10;
	}
      }
      else if ((X == 1 || X == 6) && (Y == 1 || Y == 6)){
	score += 20;
      }
    }
  }

  return score;
}
