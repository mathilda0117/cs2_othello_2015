#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
 public:
  Side own;
  Side other;
  Board board;

  Player(Side side);
  ~Player();
  
  Move *doMove(Move *opponentsMove, int msLeft);
  Move *minimax(Board *board, Side side, int level);
  int moveScore(Board *testBoard, Move *move, Side side);
  
  // Flag to tell if the player is running within the test_minimax context
  bool testingMinimax;
};

#endif
